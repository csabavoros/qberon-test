<?php

class Order
{
    private $lat;

    private $long;

    private $baseShippingFee;

    public $shippingFee;

    private $orderStock = 0;

    public function __construct(float $lat, float $long, int $baseShippingFee)
    {
        $this->lat = $lat;
        $this->long = $long;
        $this->baseShippingFee = $baseShippingFee;
    }

    /**
     * Szállítási költség kiszámítása
     */
    public function calculateShippingFee(Warehouse $warehouse1, Warehouse $warehouse2, Warehouse $warehouse3): void
    {
        $this->calculateWarehouseDistances($warehouse1);
        $this->calculateWarehouseDistances($warehouse2);
        $this->calculateWarehouseDistances($warehouse3);
        $warehouses = [$warehouse1, $warehouse2, $warehouse3];

        usort($warehouses, function ($warehouse1, $warehouse2) {
            return strcmp($warehouse1->distance, $warehouse2->distance);
        });

        $warehouseShippingCounter = $this->calculateShippingCount($warehouses);

        $this->shippingFee = $this->baseShippingFee + (($this->baseShippingFee * 0.15) * $warehouseShippingCounter);
    }

    /**
     * Kiszámolja raktár és a leadott rendelés hely közötti távolságot
     * 
     * @param Warehouse $warehouse
     * @return void
     */
    private function calculateWarehouseDistances(Warehouse $warehouse): void
    {
        $theta = $warehouse->getLong() - $this->long;
        $dist = sin(deg2rad($warehouse->getLat())) * sin(deg2rad($this->lat)) +  cos(deg2rad($warehouse->getLat())) * cos(deg2rad($this->lat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $warehouse->distance = $dist * 60 * 1.1515 * 1.609344;
    }

    /**
     * Kiszámolja mennyi raktár között kell szállítanunk az árút
     * 
     * @param array $warehouses
     * @return int
     */
    private function calculateShippingCount(array $warehouses): int
    {
        $warehouseShippingCounter = 0;

        foreach ($warehouses as $warehouse) {

            if ($this->orderStock < 10) {
                $this->orderStock += $warehouse->getStock();
                if ($this->orderStock > 10) {
                    $this->orderStock = $this->orderStock - ($this->orderStock - 10);
                    break;
                }
                $warehouseShippingCounter++;
            }
        }

        return $warehouseShippingCounter;
    }
}
