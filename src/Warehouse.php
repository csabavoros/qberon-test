<?php

class Warehouse
{
    private $lat;

    private $long;

    private $stock;

    public $distance;

    public function __construct(float $lat, float $long, int $stock)
    {
        $this->lat = $lat;
        $this->long = $long;
        $this->stock = $stock;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getLong()
    {
        return $this->long;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function outOfStock()
    {
        return ($this->stock <= 0);
    }
}
