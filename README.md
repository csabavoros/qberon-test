# QBeron teszt feladat #

A program, rendelés leadását követően számítja ki a szállítási költséget, az adott raktárak távolságától függően.

### Telepítés ###

* A repo leklónozását követően `composer install`-al települ a PHPUnit könyvtár, amivel lehet futtatni a teszteket.
* A tesztek a `/tests` könyvtárban találhatóak

### Tesztek ###

* A rendelésen belül 3 teszt esetet vizsgáltam, ezeket egyedileg paramétereztem
* Írtam tesztet a raktárkészlet helyes kiszámításásra

#### Rendelés paraméterek ####

Az `Order` osztálynak 3 paramétere van.

`lat`: A rendelés helyének a koordinátája

`long`: A rendelés helyének a koordinátája

`baseShippingFee`: Alap szállítási költség, amiből lesz számolva a végösszeg

#### Raktár paraméterek ####

A `Warehouse` osztálynak 3 paramétere van.

`lat`: A raktár helyének a koordinátája

`long`: A raktár helyének a koordinátája

`stock`: A raktárkészlet mennyisége