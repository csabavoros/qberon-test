<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class WarehouseTest extends TestCase
{
    public function testOutOfStock()
    {
        $warehouse = new Warehouse(47.510323727951466, 19.055773702156678, 0);

        $this->assertEquals(
            true,
            $warehouse->outOfStock()
        );

    }
}
