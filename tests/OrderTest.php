<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class OrderTest extends TestCase
{
    public function testOrderingCase1()
    {
        $order = new Order(47.49710840317527, 19.07053906905496, 1000);

        $warehouse1 = new Warehouse(47.510323727951466, 19.055773702156678, 4);
        $warehouse2 = new Warehouse(47.50514981166191, 19.06333239035631, 5);
        $warehouse3 = new Warehouse(47.50128837671594, 19.067948839851905, 1);
    
        $order->calculateShippingFee($warehouse1, $warehouse2, $warehouse3);

        $this->assertEquals(
            1450,
            $order->shippingFee
        );
    }

    public function testOrderingCase2()
    {
        $order = new Order(47.50204933448029, 19.085392476576065, 1000);

        $warehouse1 = new Warehouse(48.15453181549978, 17.141577919738815, 15);
        $warehouse2 = new Warehouse(49.23394175484712, 16.617795529009378, 5);
        $warehouse3 = new Warehouse(48.251503286770635, 16.28606656599157, 4);

        $order->calculateShippingFee($warehouse1, $warehouse2, $warehouse3);

        $this->assertEquals(
            1000,
            $order->shippingFee
        );
    }

    public function testOrderingCase3()
    {
        $order = new Order(47.4792861128873, 19.130512951178126, 5000);

        $warehouse1 = new Warehouse(49.032023989306865, 21.296771047841972, 7);
        $warehouse2 = new Warehouse(49.69011247342485, 21.788158783451724, 8);
        $warehouse3 = new Warehouse(51.127867200484886, 17.005780472939918, 12);

        $order->calculateShippingFee($warehouse1, $warehouse2, $warehouse3);

        $this->assertEquals(
            5750,
            $order->shippingFee
        );
    }
}